# 拖拽排序列表

![演示](https://images.gitee.com/uploads/images/2020/0116/104224_9f3ad79f_1472286.gif "20200115165248835 (1).gif")

## 思路
界面分为两层：
    - 底层，正常列表展示，拖拽的时候不做处理（大牛直接加了动画，原谅我技艺不精，还没实现）
    - 顶层，movable-view组件，不长按不展示，之后长按才展示，且没有点击事件。

## 事件
主要监听：`longpress` , `touchmove` , `touchend` 三个[事件](https://developers.weixin.qq.com/miniprogram/dev/framework/view/wxml/event.html#%E4%BA%8B%E4%BB%B6%E8%AF%A6%E8%A7%A3)

### longpress
保障长按才有效，并设定许多其他值。

### touchmove
滑动的时候触发
1. 判断是否需要滑动页面，因为 `movable-area`组件 滑动事件被catch掉，无法滑动；
2. 记录滑动经过的项，在页面【底层】设置经过项为其他样式（算是补足没有滑动动画的缺陷吧，不需要可以删除 `lastTarget`字段）。

### touchend
滑动结束时触发
1. 获取目标节点索引，将初始拖拽节点移动至目标节点，其中的关键代码为：
```js
list.splice(futureIndex, 0, list.splice(dragElementIndex, 1)[0]);  // 移动位置
```
> (这行代码参考了[Vue.Draggable](https://github.com/SortableJS/Vue.Draggable/blob/2df59aecda6901d56a83bfd299e67973482271c5/src/vuedraggable.js#L354)中一行代码)

2. 将其他设置置空


## 代码片段

[代码片段](https://developers.weixin.qq.com/s/GIGjDpmz7yeH)



## 参考链接

[1] SortableJS [Vue.Draggable](https://github.com/SortableJS/Vue.Draggable)

[2] SortableJS [Sortable](https://github.com/SortableJS/Sortable)

[3] SortableJS演示示例 [Vue.Draggable](https://sortablejs.github.io/Vue.Draggable/#/transition-example)

[4] 好想再胖十斤 [25行代码解决小程序的拖拽排序](https://blog.csdn.net/qq_33719683/article/details/79767090)